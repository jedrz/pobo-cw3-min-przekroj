Solver do ćw. 3 z pobo.

Znajduje minimalny przekrój sieci.

Użycie:

    min_przekroj.py <ścieżka do pliku z grafem w formacie modgraf>
    [nazwa wierzchołka początkowego (domyś. S)]
    [nazwa wierzchołka dolecowego (domyś. T)]

Sprawdź też [ten solver](https://bitbucket.org/Linkas/5x5pub/), wyznaczający
najlepszy przydział w zadaniu 2.
