#!/usr/bin/env python
# -*- coding: utf-8 -*-

import itertools
import sys


INF = 10 ** 10


class Edge:

    def __init__(self, start, end, val):
        self.start = start
        self.end = end
        self.val = val

    def __str__(self):
        return '{} -> {} [{}]'.format(self.start, self.end, self.val)


def all_partitions(iterable):
    """Generuje wszystkie podziały zbioru na 2 zbiory."""
    iterable_set = set(iterable)
    for combination_len in range(1, len(iterable) // 2 + 1):
        for combination in itertools.combinations(iterable_set,
                                                  combination_len):
            combination_set = set(combination)
            yield (combination_set, iterable_set - combination_set)


def find_edge_for_vertices(vertices, edges):
    """Zwraca krawędź dla podanego wierzchołka."""
    for edge in edges:
        if tuple(vertices) == (edge.start, edge.end):
            return edge
    return None


def intersection_throughput(partition, edges):
    """Oblicza przepustowość przekroju."""
    first_set, second_set = partition[0], partition[1]
    result = 0
    for first_set_v in first_set:
        for second_set_v in second_set:
            edge = find_edge_for_vertices((first_set_v, second_set_v), edges)
            if edge:
                result += edge.val
    return result


def find_min_intersection(edges, start='S', target='T'):
    """Znajduje minimalny przekrój, tj. taki którego przepustowość jest
    najmniejsza spośród wszystkich przekrójów oraz zawiera wierzchołek
    `start` (domyślnie 'S') w zbiorze S oraz
    `target` (domyślnie 'T') w zbiorze T.
    Zwraca ten przekrój oraz jego przepustowość.
    """
    vertices = set()
    for edge in edges:
        vertices.add(edge.start)
        vertices.add(edge.end)

    def is_better_intersection(old_throughput, new_throughput, partition):
        return old_throughput > new_throughput \
            and start in partition[0] \
            and target in partition[1]

    min_throughput = INF
    min_intersection = None
    for partition in all_partitions(vertices):
        throughtput = intersection_throughput(partition, edges)
        if is_better_intersection(min_throughput, throughtput, partition):
            min_throughput = throughtput
            min_intersection = partition
        reversed_partition = list(reversed(partition))
        reversed_throughtput = intersection_throughput(reversed_partition,
                                                       edges)
        if is_better_intersection(min_throughput, reversed_throughtput,
                                  reversed_partition):
            min_throughput = reversed_throughtput
            min_intersection = reversed_partition
    return (min_intersection, min_throughput)


def parse_file(path):
    """Parsuje opis grafu w formacie dla programu modgraf."""
    with open(path) as desc:
        edges = []
        # Pomiń linie wstępu
        lines = desc.readlines()[4:]
        for line in lines:
            partitioned_line = line.split()
            start = partitioned_line[0]
            end = partitioned_line[1]
            val = int(partitioned_line[2])
            edge = Edge(start, end, val)
            edges.append(edge)
    return edges


def main(path, start='S', target='T'):
    edges = parse_file(path)
    for e in edges:
        print(e)
    print(find_min_intersection(edges, start, target))


if __name__ == '__main__':
    if not 2 <= len(sys.argv) <= 4:
        print("Użycie: {} <ścieżka do pliku z grafem w formacie modgraf> "
              "[nazwa wierzchołka początkowego (domyś. S)] "
              "[nazwa wierzchołka dolecowego (domyś. T)]".format(sys.argv[0]))
        sys.exit(1)
    start = 'S'
    if len(sys.argv) >= 3:
        start = sys.argv[2]
    target = 'T'
    if len(sys.argv) == 4:
        target = sys.argv[3]
    main(sys.argv[1], start, target)
